<?php
/*
 * The MIT License
 *
 * Copyright 2015 fabien.sanchez.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$a = "
select
    OL_PRODUIT.PR_CODE_PRODUIT \"PRODUIT\",
    OL_VERSION.VE_NUM \"VERSION\"
from
    OL_VERSION,
    OL_PRODUIT_DOMAINE,
    OL_PRODUIT
where
    OL_VERSION.ID_PRODUIT_DOMAINE = OL_PRODUIT_DOMAINE.ID_PRODUIT_DOMAINE
    and OL_PRODUIT_DOMAINE.ID_PRODUIT = OL_PRODUIT.ID_PRODUIT
union
select
    'HOL_ENV',
    VE_NUM
from
    OL_VERSION
where
    OL_VERSION.ID_PRODUIT_DOMAINE = 0 OR OL_VERSION.ID_PRODUIT_DOMAINE IS NULL
";

$titre = 'Applications metier';
Vue::addFileStyle('./form.css');
Vue::addFileStyle('./bouton.css');
Vue::addFileStyle('./import.css');
Vue::addScript("var URL_MAJSCHEMA = '" . url('ajax_schema') . "'," . PHP_EOL
    . "URL_MAJTABLE = '" . url('ajax_table') . "'," . PHP_EOL
    . "TK_CSRF = '" . getCsrf() . "';");
Vue::addFileScript('./app_metier_forms.js');

$cnx = Box::get('Db');
$schema = $cnx->prepare('SELECT * FROM ALL_USERS WHERE USER_ID > 36');
$schema->execute();
$user = get('user', session('user', ''));
$mdp = get('mdp', session('mdp', ''));
$mdp = empty($mdp) ? $user : $mdp;
include './page/app_metier/forms_appmetier.php';
?>
<form class="js_user">
    <div class="group">
        <label for="user"> Choisir le schema : </label>
        <select name = "user" id = "user">
            <option></option>
            <?php while (false !== ($row = $schema->fetchObject())):
                ?>
                <option <?php
                if ($user == $row->USERNAME) {
                    echo 'selected';
                }
                ?>><?= $row->USERNAME; ?></option>
                <?php endwhile; ?>
        </select>

    </div>
    <div class="group">
        <label for="mdp"> Mot de passe : </label>
        <input type="password" id="mdp" name="mdp" value="<?= $mdp; ?>">
    </div>
</form>
<div class="js_form_sec">
    <?php
    affiche_forms($cnx, $user, $mdp);
    ?>
</div>