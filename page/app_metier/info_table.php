<?php
/*
 * The MIT License
 *
 * Copyright 2015 fabien.sanchez.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

Vue::addFileStyle('./info-table.css');
Vue::addFileStyle('./bouton.css');

if (isPostMethode()) {
    checkPostCsrf();

    $user = post('user');
    $mdp = post('mdp', $user);
    $mdp = empty($mdp) ? $user : $mdp;
    $table = post('table');

    try {
        $cnx = Box::get('Db');
        $infos = $cnx->prepare("SELECT DISTINCT ALL_TAB_COLUMNS.COLUMN_NAME,
            ALL_TAB_COLUMNS.DATA_TYPE, ALL_TAB_COLUMNS.DATA_LENGTH,
            ALL_TAB_COLUMNS.DATA_PRECISION, ALL_TAB_COLUMNS.DATA_SCALE,
            ALL_TAB_COLUMNS.CHAR_LENGTH, ALL_TAB_COLUMNS.NULLABLE,
            ALL_CONSTRAINTS.CONSTRAINT_TYPE, ALL_INDEXES.UNIQUENESS,
            ALL_TAB_COLUMNS.COLUMN_ID
            FROM
            ALL_TAB_COLUMNS
            LEFT OUTER JOIN ALL_CONS_COLUMNS
            ON (ALL_TAB_COLUMNS.OWNER = ALL_CONS_COLUMNS.OWNER
            AND ALL_TAB_COLUMNS.TABLE_NAME = ALL_CONS_COLUMNS.TABLE_NAME
            AND ALL_TAB_COLUMNS.COLUMN_NAME = ALL_CONS_COLUMNS.COLUMN_NAME)
            LEFT OUTER JOIN ALL_IND_COLUMNS
            ON (ALL_TAB_COLUMNS.OWNER = ALL_IND_COLUMNS.TABLE_OWNER
            AND ALL_TAB_COLUMNS.COLUMN_NAME = ALL_CONS_COLUMNS.COLUMN_NAME
            AND ALL_TAB_COLUMNS.COLUMN_NAME = ALL_IND_COLUMNS.COLUMN_NAME
            AND ALL_TAB_COLUMNS.TABLE_NAME = ALL_IND_COLUMNS.TABLE_NAME)
            LEFT OUTER JOIN ALL_INDEXES
            ON (ALL_IND_COLUMNS.INDEX_NAME = ALL_INDEXES.INDEX_NAME
            AND ALL_IND_COLUMNS.INDEX_OWNER = ALL_INDEXES.OWNER
            AND ALL_IND_COLUMNS.TABLE_NAME = ALL_INDEXES.TABLE_NAME
            AND ALL_IND_COLUMNS.TABLE_OWNER = ALL_INDEXES.TABLE_OWNER)
            LEFT OUTER JOIN SYS.ALL_CONSTRAINTS
            ON (ALL_CONS_COLUMNS.OWNER = ALL_CONSTRAINTS.OWNER
            AND ALL_CONS_COLUMNS.TABLE_NAME = ALL_CONSTRAINTS.TABLE_NAME
            AND ALL_CONS_COLUMNS.CONSTRAINT_NAME = ALL_CONSTRAINTS.CONSTRAINT_NAME)
            WHERE
            upper(ALL_TAB_COLUMNS.OWNER) = upper(?)
            AND upper(ALL_TAB_COLUMNS.TABLE_NAME) = upper(?)
            ORDER BY ALL_TAB_COLUMNS.COLUMN_ID");
        $infos->execute([$user, $table]);
        ?>
        <div><a class="btn" href="<?= urlGoBack(); ?>">&Lt;</a></div>
        <div class="table">
            <div class="table_name"><?= $table; ?></div>
            <?php
            while (false !== ($row = $infos->fetchObject())) :
                $class = '';
                $class .= ($row->CONSTRAINT_TYPE == "P") ? ' pk' : '';
                $class .= ($row->CONSTRAINT_TYPE == "R") ? ' fk' : '';
                $class .= (!is_null($row->UNIQUENESS)) ? ' ix' : '';
                $class .= ($row->UNIQUENESS == "UNIQUE") ? ' uq' : '';
                ?>
                <div class="column<?= $class; ?>">
                    <span class="column_name"><?= $row->COLUMN_NAME; ?></span>
                    <span class="data_type">
                        <span class="type_name"><?= $row->DATA_TYPE; ?></span>
                        <?php
                        switch ($row->DATA_TYPE) {
                            case "NUMBER":
                                ?>
                                (
                                <span class="data_precision"><?= $row->DATA_PRECISION; ?></span>,
                                <span class="data_scale"><?= $row->DATA_SCALE; ?></span>
                                )
                                <?php
                                break;
                            case "FLOAT":
                                ?>
                                (
                                <span class="data_precision"><?= $row->DATA_PRECISION; ?></span>
                                )
                                <?php
                                break;
                            case "VARCHAR2":
                                ?>
                                (
                                <span class="data_length"><?= $row->DATA_LENGTH; ?></span>
                                )
                                <?php
                                break;
                            case "CLOB":
                            case "BLOB":
                            case "DATE":
                                break;
                            default:
                                ?>
                                (
                                <span class="data_length"><?= $row->DATA_LENGTH; ?></span>,
                                <span class="data_precision"><?= $row->DATA_PRECISION; ?></span>,
                                <span class="data_scale"><?= $row->DATA_SCALE; ?></span>
                                )
                            <?php
                        }
                        ?>
                    </span>
                </div>
            <?php endwhile; ?>
        </div>
        <?php
    } catch (PDOException $e) {
        setFlash('erreur', "un problème est survenu lors de la mise à jour du login mot de pass !");
        redirect(500, url('appmetier'));
    }
}