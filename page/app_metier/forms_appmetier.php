<?php
/*
 * The MIT License
 *
 * Copyright 2015 fabien.sanchez.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

function form_maj_login_mdp($user = '', $mdp = '')
{
    ?>
    <a name="majlogmdp"></a>
    <form action="<?= url('app_majloginmdp'); ?>" method="POST">
    <?= inputCsrf(); ?>
        <input type="hidden" name="user" value="<?= $user; ?>">
        <input type="hidden" name="mdp" value="<?= $mdp; ?>">
        <button type = "submit" class = "btn btn-principal">M.A.J. des login, mots de passe</button>
    </form>
    <?php
}

function form_get_log_xml($user = '', $mdp = '')
{
    ?>
    <a name="getlogxml"></a>
    <form class="js_logxml" action="<?= url('app_logxml'); ?>" method="POST">
    <?= inputCsrf(); ?>
        <input type="hidden" name="user" value="<?= $user; ?>">
        <input type="hidden" name="mdp" value="<?= $mdp; ?>">
        <div class="group">
            <label for="sid">SID utilisé pour ateindre la base</label>
            <input type="text" name="sid" id="sid" value="" required="required"/>
        </div>
        <div class="group">
            <label for="produit">Produit</label>
            <select name="produit" id="produit" required="required"></select>
        </div>
        <div class="group">
            <label for="entite">Entite</label>
            <select name="entite" id="entite" required="required"></select>
        </div>
        <div class="group">
            <label for="agent">Agent</label>
            <select name="agent" id="agent" required="required"></select>
        </div>
        <button type="submit" class = "btn btn-principal">génère le log.xml</button>
    </form>
    <div id="sub_getlogxml"></div>
    <?php
}

function form_get_info_table($user = '', $mdp = '')
{
    ?>
    <a name="getinfotable"></a>
    <form action="<?= url('info_table'); ?>" method="POST">
    <?= inputCsrf(); ?>
        <input type="hidden" name="user" value="<?= $user; ?>">
        <input type="hidden" name="mdp" value="<?= $mdp; ?>">
        <div class="group">
            <label for="table">Informations su la table</label>
            <input type="text" name="table" id="table" value="" />
        </div>
        <button type = "submit" class = "btn btn-principal">desc de la table</button>
    </form>
    <?php
}

function form_maj_version(PDO $db, $user = '', $mdp = '')
{
    ?>
    <a name="majversion"></a>
    <form action="<?= url('app_majversion'); ?>" method="POST">
    <?= inputCsrf(); ?>
        <input type="hidden" name="user" value="<?= $user; ?>">
        <input type="hidden" name="mdp" value="<?= $mdp; ?>">
        <div class="js_version" style="margin: 5px 0">
        </div>
        <button type = "submit" class = "btn btn-principal">M.A.J. des versions</button>
    </form>
    <?php
}

function affiche_forms(PDO $db, $user = '', $mdp = '')
{
    $mdp = empty($mdp) ? $user : $mdp;
    form_maj_login_mdp($user, $mdp, $mdp);
    form_get_log_xml($user, $mdp, $mdp);
    form_maj_version($db, $user, $mdp);
    form_get_info_table($user, $mdp);
}
