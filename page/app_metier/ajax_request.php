<?php

/*
 * The MIT License
 *
 * Copyright 2015 fabien.sanchez.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

function ajax_schema() {
    if (isAjaxMethode()) {
        checkPostCsrf();
        $user = post('user');
        $mdp = post('mdp', '');
        $mdp = empty($mdp) ? $user : $mdp;
        $_SESSION['user'] = $user;
        $_SESSION['mdp'] = $mdp;
        try {
            $cnx = Box::get('Db');
            $req = "select OL_PRODUIT.ID_PRODUIT \"ID\", OL_PRODUIT.PR_CODE_PRODUIT \"LIBELLE\" from $user.OL_PRODUIT ORDER BY ID_PRODUIT";
            $produit_stm = $cnx->query($req);
            if ($produit_stm === false) {
                setFlash(FLASH_ERROR, "Erreur dans l'éxécution de la requète : " . $req);
                redirect(500, url('appmetier'));
            }
            $produit = $produit_stm->fetchAll(PDO::FETCH_ASSOC);
            $req = "SELECT ID_ENTITE \"ID\", EN_CODE \"LIBELLE\" FROM $user.OL_ENTITE";
            $entite_stm = $cnx->query($req);
            if ($entite_stm === false) {
                setFlash(FLASH_ERROR, "Erreur dans l'éxécution de la requète : " . $req);
                redirect(500, url('appmetier'));
            }
            $entite = $entite_stm->fetchAll(PDO::FETCH_ASSOC);
            $req = "SELECT ID_LOGIN \"ID\", LO_ADMINISTRATEUR \"ADMIN\", LO_PRENOM || ' ' || LO_NOM || ' (' || LO_LOGIN || ')' \"NOM\" FROM $user.OL_LOGIN order by ID_LOGIN";
            $login_stm = $cnx->query($req);
            $login = $login_stm->fetchAll(PDO::FETCH_ASSOC);
            if ($login_stm === false) {
                setFlash(FLASH_ERROR, "Erreur dans l'éxécution de la requète : " . $req);
                redirect(500, url('appmetier'));
            }
            $req = "SELECT NVL(PR_CODE_PRODUIT,'HOL_ENV') \"PRODUIT\", VE_NUM \"VERSION\", ID_VERSION \"ID\" FROM {$user}.OL_PRODUIT INNER JOIN {$user}.OL_PRODUIT_DOMAINE ON (OL_PRODUIT.ID_PRODUIT = OL_PRODUIT_DOMAINE.ID_PRODUIT) RIGHT OUTER JOIN  {$user}.OL_VERSION ON (OL_PRODUIT_DOMAINE.ID_PRODUIT_DOMAINE = OL_VERSION.ID_PRODUIT_DOMAINE)";
            $version_stm = $cnx->query($req);
            $version = $version_stm->fetchAll(PDO::FETCH_ASSOC);
            if ($version_stm === false) {
                setFlash(FLASH_ERROR, "Erreur dans l'éxécution de la requète : " . $req);
                redirect(500, url('appmetier'));
            }
            $_SESSION['USER'] = $user;
            $_SESSION['MDP'] = $mdp;
            header('Content-Type: application/json');
            $data = compact('produit', 'entite', 'login', 'version');
            echo MyJsonEncode($data, [ 'STRING2HTML' => true]);
        } catch (PDOException $e) {
            setFlash(FLASH_ERROR, "Une exception est survenue lors de l'acces au données" . PHP_EOL . $e->getMessage());
            redirect(500, url('appmetier'));
        } catch (Exception $e) {
            setFlash(FLASH_ERROR, "Une exception générale est survenue" . PHP_EOL . $e->getMessage());
            redirect(500, url('appmetier'));
        }
    }
}

function ajax_table() {
    if (isAjaxMethode()) {
        checkPostCsrf();
        $cnx = Box::get('Db');
        $user = post('user', '');
        $request = post('request', '');

        $filtre1 = '%' . implode('%', explode(' ', $request)) . '%';
        $filtre2 = '%' . implode('%', str_split(str_replace(' ', '', $request))) . '%';

        $req = "WITH MASTER_REQUEST AS( SELECT TABLE_NAME , '1' FROM ALL_TABLES WHERE OWNER = ? AND UPPER(TABLE_NAME) LIKE(UPPER(?)) UNION SELECT TABLE_NAME , '2' FROM ALL_TABLES WHERE OWNER = ? AND UPPER(TABLE_NAME) LIKE(UPPER(?))) SELECT TABLE_NAME FROM MASTER_REQUEST GROUP BY TABLE_NAME ORDER BY COUNT(TABLE_NAME) DESC, LENGTH(TABLE_NAME), TABLE_NAME";
        $version_stm = $cnx->prepare($req);
        if (!$version_stm->execute([$user, $filtre1, $user, $filtre2])) {
            setFlash(FLASH_ERROR, "Erreur dans l'éxécution de la requète : " . print_r($version_stm->errorInfo(), true));
            redirect(500, url('appmetier'));
        }
        $tables = $version_stm->fetchAll(PDO::FETCH_ASSOC);
        header('Content-Type: application/json');
        echo MyJsonEncode($tables, [ 'STRING2HTML' => true]);
    }
}
