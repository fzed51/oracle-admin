<?php

/*
 * The MIT License
 *
 * Copyright 2015 fabien.sanchez.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

function maj_login_mdp() {

    if (isPostMethode()) {
        checkPostCsrf();
        $user = post('user');
        $mdp = post('mdp', $user);
        $mdp = empty($mdp) ? $user : $mdp;
        try {
            $cnx = Box::get('Db');
            $update = $cnx->prepare("update $user.ol_login set lo_baseuser=?, lo_basepsw=?");
            if (!$update->execute([$user, $mdp])) {
                setFlash('erreur', "un problème est survenu lors de la mise à jour de la table OL_LOGIN !");
                redirect(500, url('appmetier'));
            }
            unset($update);
            $update = $cnx->prepare("update $user.ol_domaine set do_baseuser=?, do_basepsw=?, do_baseserveur=?");
            if (!$update->execute([$user, $mdp, DB_TNS])) {
                setFlash('erreur', "un problème est survenu lors de la mise à jour de la table OL_DOMAINE !");
                redirect(500, url('appmetier'));
            }
            setFlash('succes', "mise à jour du login mot de pass effectuée !");
            redirect(200, url('appmetier'));
        } catch (PDOException $e) {
            setFlash('erreur', "un problème est survenu lors de la mise à jour du login mot de pass !");
            redirect(500, url('appmetier'));
        }
    }
}

function get_log_xml() {
    if (isPostMethode()) {
        checkPostCsrf();
        $user = post('user');
        $mdp = post('mdp', '');
        $mdp = empty($mdp) ? $user : $mdp;
        $sid = post('sid');
        $produit = post('produit');
        $entite = post('entite');
        $agent = post('agent');
        try {
            $cnx = Box::get('Db');
            $req = "SELECT
    DOM1.DO_BASEUSER,
	DOM1.DO_BASEPSW,
	DOM1.ID_DOMAINE,
	DOM1.DO_LIBELLE,
	NULLIF(DOM1.ID_DOMAINE_PERE,1) \"ID_DOMAINE_PERE\",
	DOM2.DO_LIBELLE \"DO_LIBELLE_PERE\",
	OL_LOGIN.ID_LOGIN,
	OL_LOGIN.LO_NOM,
	OL_LOGIN.LO_PRENOM,
	OL_PRODUIT_DOMAINE.ID_PRODUIT_DOMAINE,
	OL_ENTITE.ID_ENTITE,
	OL_ENTITE.EN_CODE,
	DOM1.DO_CODECLIENT,
	DOM1.DO_NOMCLEINT
FROM
    $user.OL_PRODUIT
    , $user.OL_PRODUIT_DOMAINE
    , $user.OL_VERSION
    , $user.OL_DOMAINE dom1
    , $user.OL_DOMAINE dom2
    , $user.OL_ENTITE
    , $user.OL_LOGIN
WHERE
    OL_PRODUIT.ID_PRODUIT = OL_PRODUIT_DOMAINE.ID_PRODUIT
    AND OL_PRODUIT_DOMAINE.ID_PRODUIT_DOMAINE = OL_ENTITE.ID_PRODUIT_DOMAINE
    AND DOM1.ID_DOMAINE = OL_PRODUIT_DOMAINE.ID_DOMAINE
    AND OL_PRODUIT_DOMAINE.ID_PRODUIT_DOMAINE = OL_VERSION.ID_PRODUIT_DOMAINE
    AND DOM2.ID_DOMAINE (+)= DOM1.ID_DOMAINE_PERE
    AND DOM1.DO_BASEUSER = LO_BASEUSER
    AND DOM1.DO_BASEPSW = LO_BASEPSW
    AND OL_PRODUIT.ID_PRODUIT = ?
    AND OL_ENTITE.ID_ENTITE = ?
    AND OL_LOGIN.ID_LOGIN = ?
ORDER BY
    ID_LOGIN";
            $log_stm = $cnx->prepare($req);
            $log_stm->execute([$produit, $entite, $agent]);
            if ($log_stm === false) {
                setFlash(FLASH_ERROR, "Erreur dans l'éxécution de la requète : " . $req);
                redirect(500, url('appmetier'));
            }
            $log = $log_stm->fetchAll(PDO::FETCH_ASSOC);
            if (count($log) != 1) {
                $out = print_r($log, true);
            } else {
            $log = $log[0];
            $out = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>
    <OnLine><BaseNom></BaseNom><BaseProvider>OraOLEDB.Oracle</BaseProvider><BaseServeur>$sid</BaseServeur>
    <BaseUser>{$log['DO_BASEUSER']}</BaseUser><BasePSW>{$log['DO_BASEPSW']}</BasePSW>
    <CheminProduit></CheminProduit><DomaineID>{$log['ID_DOMAINE']}</DomaineID><DomaineNom>{$log['DO_LIBELLE']}</DomaineNom>
    <DomainePereID>{$log['ID_DOMAINE_PERE']}</DomainePereID><DomainePereNom>{$log['DO_LIBELLE_PERE']}</DomainePereNom>
    <UtilisateurID>{$log['ID_LOGIN']}</UtilisateurID><UtilisateurNOMPRENOM>{$log['LO_NOM']}|{$log['LO_PRENOM']}</UtilisateurNOMPRENOM>
    <ProduitDomaine>{$log['ID_PRODUIT_DOMAINE']}</ProduitDomaine>
    <IdDernEntite>{$log['ID_ENTITE']}</IdDernEntite><CodeDernEntite>{$log['EN_CODE']}</CodeDernEntite>
    <CodeClient>{$log['DO_CODECLIENT']}</CodeClient><NomClient>{$log['DO_NOMCLEINT']}</NomClient></OnLine>";
            }
            header("Content-disposition: attachment; filename=log.xml");
            header("Content-Type: application/force-download");
            header("Content-Transfer-Encoding: application/xml\n");
            header("Content-Length: " . strlen($out));
            header("Pragma: no-cache");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
            header("Expires: 0");

            echo $out;
        } catch (PDOException $e) {
            setFlash(FLASH_ERROR, "Une exception est survenue lors de l'acces au données" . PHP_EOL . $e->getMessage());
            redirect(500, url('appmetier'));
        } catch (Exception $e) {
            setFlash(FLASH_ERROR, "Une exception générale est survenue" . PHP_EOL . $e->getMessage());
            redirect(500, url('appmetier'));
        }
    }
}

function maj_version() {
    if (isPostMethode()) {
        checkPostCsrf();

        $user = post('user');
        $version = post('version', ['id' => [], 'num' => []]);
        try {

            if (count($version['id']) <> count($version['num'])) {
                throw new Exception('Des versions de produits ne sont pas renseignées !');
            }

            $cnx = Box::get('Db');
            $version_stm = $cnx->prepare("select VE_NUM \"NUMERO\" from {$user}.OL_VERSION where ID_VERSION = ?");
            $err = 0;
            foreach ($version['id'] as $key => $id) {
                if (!isset($version['num'][$key])) {
                    throw new Exception('Des versions de produits ne correspondent pas !');
                }

                $version_stm->execute([$id]);
                $ver = $version_stm->fetchObject()->NUMERO;
                $newVer = $version['num'][$key];
                if ($ver != $newVer) {
                    $newVerQuote = $cnx->quote($newVer);
                    if ($cnx->exec("UPDATE {$user}.OL_VERSION SET VE_NUM = $newVerQuote WHERE ID_VERSION = $id") > 0) {
                        setFlash(FLASH_SUCCES, "La version {$version['prod'][$key]} a bien été modifiée en $newVer.");
                    } else {
                        setFlash(FLASH_ERROR, "Une problème est survenu lors de la modification de la versin !");
                        $err ++;
                    }
                }
            }
            if ($err > 0) {
                redirect(500, url('appmetier'));
            } else {
                redirect(200, url('appmetier'));
            }
        } catch (PDOException $e) {
            setFlash(FLASH_ERROR, "Une exception est survenue lors de l'acces au données" . PHP_EOL . $e->getMessage());
            redirect(500, url('appmetier'));
        } catch (Exception $e) {
            setFlash(FLASH_ERROR, "Une exception générale est survenue" . PHP_EOL . $e->getMessage());
            redirect(500, url('appmetier'));
        }
    }
}
