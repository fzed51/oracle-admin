<?php
$titre = 'Mentions';
$conn = Box::get('Db');
?>
<h1>Mentions CAP</h1>
<?php
try {
    $stmt = $conn->query('SELECT * FROM AS_CAP_MENTION ORDER BY ID_CAP_MENTION');
    $result = $stmt->fetchAll();
    Vue::addStyle("
		pre td { border: 1px solid #bbb; padding:0.1em 0.2em; }
		pre th { border: 2px solid #888; padding:0.1em 0.2em; }
		pre table {
			border-collapse: collapse;
			border : 4px double #888;
		}
	");
    echo '<pre>';
    echo fetchall2table($result, ['ID_CAP_MENTION', 'MEN_LIBELLE', 'MEN_MULTITYPE']);
    echo '</pre>';
} catch (PDOException $e) {
    afficheErreurException($e);
}