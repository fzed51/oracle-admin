<?php
$titre = 'Familles';
Vue::addFileStyle('./famille.css');
$conn = Box::get('Db');
try {
    ?>
    <h1>Familles</h1>
    <?php
    $stmt = $conn->query('SELECT * FROM AS_V_FAM_ADRESSE ORDER BY FAM_NOM');
    ?>
    <table>
        <tbody>
            <tr>
                <th>Nom de la famille</th>
                <th>Adresse</th>
            </tr>
            <?php
            while (false !== ($row = $stmt->fetchObject())) {
                echo '<tr>';
                echo '<td>';
                echo '<a href="' . url('famille_n', ['id' => $row->ID_FAMILLE]) . '">';
                echo html($row->FAM_NOM, ENT_COMPAT | ENT_HTML401, 'cp1252');
                echo '</a>';
                echo '</td>';
                echo '<td>';
                echo html($row->ADRESSE_FORMATE, ENT_COMPAT | ENT_HTML401, 'cp1252');
                echo '</td>';
                echo '</tr>' . PHP_EOL;
            }
            ?>
        </tbody>
    </table>
    <?php
} catch (PDOException $e) {
    afficheErreurException($e);
}

