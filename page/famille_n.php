<?php
$conn = Box::get('Db');
$id = get('id', -1);
try {
    if ($id > 0) {
        Vue::addFileStyle('./famille_n.css');
        $stmt_adr = $conn->prepare('SELECT * FROM AS_V_FAM_ADRESSE WHERE ID_FAMILLE = ?');
        $stmt_adr->execute([$id]);
        $adresse = $stmt_adr->fetchObject()->ADRESSE_FORMATE;
        $stmt_fam = $conn->prepare('SELECT * FROM AS_FAMILLE WHERE ID_FAMILLE = ?');
        $stmt_fam->execute([$id]);
        $famille = $stmt_fam->fetchObject();
        $stmt_ind = $conn->prepare('select * from AS_PERFOYER inner join AS_INDIVIDU on (AS_PERFOYER.ID_INDIVIDU = AS_INDIVIDU.ID_INDIVIDU) where AS_PERFOYER.ID_FAMILLE = ?');
        $stmt_ind->execute([$id]);
        $titre = 'Familles ' . htmlentities($famille->FAM_NOM, ENT_COMPAT | ENT_HTML401, 'cp1252');
        ?>
        <h1><a href="<?=
            urlGoBack();
            ?>">&Lt;&nbsp;</a>Familles <?= htmlentities($famille->FAM_NOM, ENT_COMPAT | ENT_HTML401, 'cp1252') ?></h1>
        <table>
            <tr>
                <td>
                    <?= htmlentities($famille->FAM_NOM, ENT_COMPAT | ENT_HTML401, 'cp1252') ?><br>
                    n° <?= $famille->ID_FAMILLE ?>
                </td>
                <td>
                    <?= nl2br(htmlentities(trim(str_replace(',', "\n", $adresse)), ENT_COMPAT | ENT_HTML401, 'cp1252')) ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php
                    while (false !== ($individu = $stmt_ind->fetchObject())) {
                        echo $individu->IND_ACC_ALPHA;
                        echo "<br>" . PHP_EOL;
                    }
                    ?>
                </td>
            </tr>
        </table>
        <?php
    }
} catch (PDOException $e) {
    afficheErreurException($e);
}

