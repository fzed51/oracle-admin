<?php

use fzed51\Core\Box;
use fzed51\Core\Menu;
use fzed51\Core\MenuLink;

Box::set('Db', function() {
    if (!defined('DB_TNS')) {
        define('DB_TNS', "ORCL");
    }
    try {
        $pdo = new PDO("oci:dbname=" . DB_TNS, 'SYSTEM', 'oracle');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    } catch (PDOException $e) {
        echo "<div class=\"error\">";
        echo "<h1>Connexion à la base de données impocible.</h1>";
        echo "<p>" . ($e->getMessage()) . "</p>";
        echo "</div>";
        exit(1);
    }
}, true);

Box::set('Menu', function () {
    return new Menu(function() {
        $this->addItem('Home', new MenuLink(url('')));
        $this->addItem('Infos', new MenuLink(url('info')));
        $this->addItem('Schema Oracle', new MenuLink(url('schema')));
        $this->addItem('Tablespaces', new MenuLink(url('tablespace')));
        $this->addItem('Import de dump', new MenuLink(url('import')));
        $this->addItem('Applications métier', new MenuLink(url('appmetier')));
    });
}, true);

Box::set('Vue', function() {
    $vue = new fzed51\Vue('./template');
}, true);
