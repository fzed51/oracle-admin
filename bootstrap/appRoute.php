<?php

$_ROUTE = [
        /* 'famille' => ['url' => 'p=famille', 'action' => function() {
          historiz();
          publish('famille.php');
          }],
          'famille_n' => ['url' => 'p=famille_n&id={id}', 'action' => function() {
          publish('famille_n.php');
          }], */
];

Route::set('home', '', function() {
    historiz();
    publish('home.php');
});

Route::set('info', 'info', function() {
    historiz();
    publish('info.php');
});

// ---------- SCHEMA ----------
Route::set('schema', 'schema', 'index@App\Controleur\SchemaControleur');
Route::set('nouv_schema', 'schema/nouveau', 'nouveau@App\Controleur\SchemaControleur');
Route::set('supp_schema', 'schema/supprime/{name}', 'supprime@App\Controleur\SchemaControleur');
Route::set('deco_schema', 'schema/deconnect/{name}', 'deconnexion@App\Controleur\SchemaControleur');

// ---------- IMPORT DE DUMP ----------
Route::set('import', 'dump/import', 'index@App\Controleur\DumpControleur');
Route::set('head_dump', 'dump/head', 'head@App\Controleur\DumpControleur');

// ---------- TABLESPACE ----------
Route::set('tablespace', 'tablespace', 'index@App\Controleur\TablespaceControleur');
Route::set('nouv_tablespace', 'tablespace/nouveau', 'nouveau@App\Controleur\TablespaceControleur');
Route::set('supp_tablespace', 'tablespace/supprime/{name}', 'supprime@App\Controleur\TablespaceControleur');

// ---------- APPLICATIN METIER ----------
Route::set('appmetier', 'appmetier', function() {
    historiz();
    publish('app_metier/index.php');
});
Route::set('app_majloginmdp', 'appmetier/majloginmdp', function() {
    csrfBack();
    include "page\app_metier\appmetier.php";
    maj_login_mdp();
});
Route::set('ajax_schema', 'appmetier/ajax/schema', function() {
    csrfBack();
    include "page\app_metier\ajax_request.php";
    ajax_schema();
});
Route::set('app_logxml', 'appmetier/logxml', function() {
    csrfBack();
    include "page\app_metier\appmetier.php";
    get_log_xml();
});
Route::set('app_majversion', 'appmetier/majversion', function() {
    csrfBack();
    include "page\app_metier\appmetier.php";
    maj_version();
});
Route::set('ajax_table', 'appmetier/ajax/table', function() {
    csrfBack();
    include "page\app_metier\ajax_request.php";
    ajax_table();
});
Route::set('info_table', 'appmetier/info_table', function() {
    historiz();
    publish("app_metier\info_table.php");
});
