/*
 * The MIT License
 *
 * Copyright 2015 fabien.sanchez.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* global TK_CSRF, URL_MAJTABLE, URL_MAJSCHEMA */

$(document).ready(function () {

    var formatMatch = function (request, data) {
        var _matchString = request.split(' '),
            _matchValid = true,
            _matchIn = new String(data),
            _matchResult = '';

        _matchString.forEach(function (_item) {
            var _pos = _matchIn.toUpperCase().indexOf(_item.toUpperCase());
            if (_pos >= 0) {
                _matchResult += _matchIn.substr(0, _pos) + '<b style="color:#000">' + _matchIn.substr(_pos, _item.length) + '</b>';
                _matchIn = _matchIn.substr(_pos + _item.length);
            } else {
                _matchValid = false;
            }
        });
        if (_matchValid)
            return _matchResult + _matchIn;

        _matchString = request.replace(' ', '').split(''),
            _matchValid = true,
            _matchIn = data,
            _matchResult = '';

        _matchString.forEach(function (_item) {
            var _pos = _matchIn.toUpperCase().indexOf(_item.toUpperCase());
            if (_pos >= 0) {
                _matchResult += _matchIn.substr(0, _pos) + '<b style="color:#000">' + _matchIn.substr(_pos, _item.length) + '</b>';
                _matchIn = _matchIn.substr(_pos + _item.length);
            } else {
                _matchValid = false;
            }
        });
        if (_matchValid)
            return _matchResult + _matchIn;
        return data;

    };
// Fonction de mise ? jour du formulaire pour le log XML
    var _index,
        updateFormLogXml = function () {
            var _user = $('.js_user select[name=user]').val(),
                _mdp = $('.js_user input[name=mdp][type=password]').val();
            $.post(URL_MAJSCHEMA, {'user': _user, 'mdp': _mdp, 'CSRF': TK_CSRF}, function (data) {
                $('select#produit').html('');
                $('<option>')
                    .text('')
                    .appendTo('select#produit');
                data.produit.forEach(function (item) {
                    $('<option>')
                        .attr('value', item.ID)
                        .text(item.LIBELLE)
                        .appendTo('select#produit');
                });
                $('select#entite').html('');
                $('<option>')
                    .text('')
                    .appendTo('select#entite');
                data.entite.forEach(function (item) {
                    $('<option>')
                        .attr('value', item.ID)
                        .text(item.LIBELLE + ' (' + item.ID + ')')
                        .appendTo('select#entite');
                });
                var em = function (item) {
                    if (item.ADMIN > 0) {
                        return '&lt;#&gt; ' + item.NOM + ' (' + item.ID + ')';
                    } else {
                        return item.NOM + ' (' + item.ID + ')';
                    }
                };
                $('select#agent').html('');
                $('<option>')
                    .text('')
                    .appendTo('select#agent');
                data.login.forEach(function (item) {
                    $('<option>')
                        .attr('value', item.ID)
                        .html(em(item))
                        .appendTo('select#agent');
                });
                $('div.js_version').html('');
                var i = 0;
                data.version.forEach(function (item) {
                    var $dev = $('<div>')
                        .addClass('group');
                    $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'version[id][]')
                        .val(item.ID)
                        .appendTo($dev);
                    $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'version[prod][]')
                        .val(item.PRODUIT)
                        .appendTo($dev);
                    $('<label>')
                        .attr('for', 'version_' + i)
                        .text(item.PRODUIT)
                        .appendTo($dev);
                    $('<input>')
                        .attr('type', 'text')
                        .attr('id', 'version_' + i)
                        .attr('name', 'version[num][]')
                        .attr('required', 'required')
                        .val(item.VERSION)
                        .appendTo($dev);
                    $dev.appendTo('div.js_version');
                    i++;
                });
            }, 'json').fail(function (a, b, c) {
                location.reload(true);
            });
        },
        update = function () {
            var _user = $('select[name=user]').val(),
                _mdp = $('input[name=mdp][type=password]').val();
            $('.js_form_sec input[name=user][type=hidden]').each(function () {
                $(this).val(_user);
            });
            $('.js_form_sec input[name=mdp][type=hidden]').each(function () {
                $(this).val(_mdp);
            });
            updateFormLogXml();
        };
    $('.js_user').on('change', 'select', function () {
        update();
    });
    $('#table').on('keyup', debounce(function (e) {
        var _form = this.form,
            _user = _form.user.value,
            _mdp = (_form.mdp.value || _form.user.value),
            $this = $(this),
            _request = $this.val(),
            _largeur = $this.width(),
            _gauche = $this.offset().left;
        if (isPrintableKey(e.keyCode)) {
            $this.siblings('.autocomplet').remove();
            _index = -1;
            if (_request !== '') {
                $.post(URL_MAJTABLE, {
                    'user': _user, 'mdp': _mdp, 'request': _request, 'CSRF': TK_CSRF}, function (data) {
                    var $result = $('<div>')
                        .addClass('autocomplet resultat')
                        .css({
                            position: 'absolute',
                            display: 'block',
                            width: (_largeur + 24) + 'px',
                            left: _gauche + 'px',
                            border: '1px solid #ccc',
                            borderRadius: '4px',
                            color: '#666'
                        });
                    data.forEach(function (item) {
                        $result.append(
                            $('<div>').html(formatMatch(_request, item.TABLE_NAME))
                            .css({
                                fontSize: '11px',
                                padding: '6px 12px'
                            }));
                    });
                    $this.after($result);
                }, 'json').fail(function (xhr) {
                    console.error(xhr);
                });
            }
        }
    }, 200))
        .on('keyup', function (e) {
            var _keyUp = 38,
                _keyDown = 40,
                _keyLeft = 37,
                _keyRight = 39,
                $this = $(this),
                $siblings = $this.siblings('.autocomplet.resultat');
            if (isNoPrintableKey(e.keyCode)) {
                if ($siblings) {
                    var $proposition = $siblings.children('div');
                    if (e.keyCode == _keyUp || e.keyCode == _keyDown) {
                        if (e.keyCode == _keyDown) {
                            _index++;
                            if (_index >= $proposition.length) {
                                _index = 0;
                            }
                        } else {
                            _index--;
                            if (_index < 0) {
                                _index = $proposition.length - 1;
                            }
                        }
                        $proposition.css('backgroundColor', 'inherit').removeClass('select');
                        $proposition.eq(_index).css('backgroundColor', '#ddd').addClass('select');
                        $this.val($proposition.eq(_index).text());
                        $this.select();
                    }
                }
            }
        })
        .on('blur', function () {
            $(this).siblings('.autocomplet.resultat').remove();
        })
        .attr('autocomplete', 'off');
    if ($('#user').val() !== '') {
        update();
    }

});