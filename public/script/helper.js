/*
 * The MIT License
 *
 * Copyright 2015 fabien.sanchez.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var debounce = function (callback, delay) {
    var _timer = null;
    return function () {
        var context = this,
            args = arguments;
        clearTimeout(_timer);
        _timer = setTimeout(function () {
            callback.apply(context, args);
        }, delay);
    };
};

var throttle = function (callback, delay) {
    var last = 0,
        _timer = null;
    return function () {
        var context = this,
            args = arguments,
            now = +(new Date());
        clearTimeout(_timer);
        if (last && now < last + delay) {
            _timer = setTimeout(function () {
                callback.apply(context, args);
            }, delay);
        } else {
            callback.apply(context, args);
            last = now;
        }
    };
};

function isNoPrintableKey(KeyCode) {
    var _lstKeyCode = [
        16, 17, 18, 19, 20,
        27,
        33, 34, 35, 36, 37, 38, 39, 40,
        44, 45,
        91, 92, 93,
        144, 145
    ];
    if (_lstKeyCode.indexOf(KeyCode) >= 0) {
        return true;
    } else {
        return false;
    }
}

function isPrintableKey(KeyCode) {
    return !isNoPrintableKey(KeyCode)
}