<?php

chdir(__DIR__);

require './vendor/autoload.php';
require './lib/src/Init.php';

Config::initializ("./bootstrap/config.php");
include './bootstrap/appBox.php';
include './bootstrap/appRoute.php';

$uri = getUrl();

Vue::addFileStyle('./nav.css');
Vue::setNav(Box::get('Menu')->render(new \App\CustomMenu()));

Route::dispatch($uri);
