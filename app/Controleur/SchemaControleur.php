<?php

namespace App\Controleur;

use App\SchemaManager;
use function checkGetCsrf;
use function checkPostCsrf;
use function get;
use function historiz;
use function isPostMethode;
use function pathToTemplate;
use function post;
use function publish;
use function redirect;
use function setFlash;
use function url;

/**
 * Description of SchemaControleur
 *
 * @author fabien.sanchez
 */
class SchemaControleur
{

    public function index()
    {
        historiz();
        $titre = 'Schemas';
        $schemas = SchemaManager::getAll();
        \Vue::addFileStyle('./table.css');
        \Vue::addFileStyle('./bouton.css');
        $data = compact('titre', 'schemas');
        publish(pathToTemplate('schema.index', true), $data);
    }

    public function deconnexion()
    {
        checkGetCsrf();

        $user = get('name', '');
        if (!empty($user)) {
            if (SchemaManager::deconnnect($user)) {
                setFlash('succes', "L'utilisateur {$user} a bien été deconnecté.");
                redirect(200, url('schema'));
            }
        }
        setFlash('error', "Impossible de supprimer l'utilisateur {$user}!");
        redirect(400, url('schema'));
    }

    public function nouveau()
    {

        if (isPostMethode()) {
            checkPostCsrf();

            $nom = post('nom');
            $dba = !!(post('dba', false));
            $mdp = post('mdp', '');
            $confirm = post('mdp_confirm', '');
            if (empty($mdp)) {
                $mdp = $nom;
                $confirm = $nom;
            }

            if ($mdp == $confirm) {
                if (SchemaManager::create($nom, $mdp, $dba)) {
                    setFlash('succes', "L'utilisateur {$nom} a bien été enregistré.");
                    redirect(200, url('schema'));
                }
            }
            setFlash('error', "Impossible d'enregistrer l'utilisateur {$nom}!");
            redirect(400, url('schema'));
        } else {

            $titre = 'Schemas';
            \Vue::addFileStyle('./form.css');
            \Vue::addFileStyle('./bouton.css');
            publish(pathToTemplate('schema.nouveau', true));
        }
    }

    public function supprime()
    {
        checkGetCsrf();
        $user = get('name', '');
        if (!empty($user)) {
            if (SchemaManager::drop($user)) {
                setFlash('succes', "L'utilisateur {$user} a bien été supprimé.");
                redirect(200, url('schema'));
            }
        }
        setFlash('error', "Impossible de supprimer l'utilisateur {$user}!");
        redirect(400, url('schema'));
    }

}
