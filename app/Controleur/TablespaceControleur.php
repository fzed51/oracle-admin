<?php

/*
 * Copyright (c) 2016 fabien.sanchez.
 */

namespace App\Controleur;

/**
 * Description of TablespaceControleur
 *
 * @author fabien.sanchez
 */
class TablespaceControleur
{

    public function index()
    {
        historiz();
        $titre = 'Tablespace';
        \Vue::addFileStyle('./table.css');
        \Vue::addFileStyle('./bouton.css');
        \Vue::addFileScript('./tablespace.js');
        $tablespaces = \App\TablespaceManager::getAll();
        $data = compact('titre', 'tablespaces');
        publish(pathToTemplate('tablespace.index', true), $data);
    }

    public function nouveau()
    {
        historiz();
        if (isPostMethode()) {
            checkPostCsrf();
            $cnx = \Box::get('Db');

            $tblspc = post('nom');
            $size = post('size');
            $extend = !!post('extend', false);
            $extendSize = post('extend_size');
            $noLimit = !!post('no_limit', false);
            $limitSize = post('limit_size');

            $req = "CREATE TABLESPACE {$tblspc} DATAFILE 'C:\ORACLE\ORADATA\ORCL\\{$tblspc}01.DBF'";
            $req .= " SIZE {$size}";
            if (!$extend) {
                $req .= " AUTOEXTEND OFF";
            } else {
                $req .= " AUTOEXTEND ON NEXT {$extendSize}";
                if (!$noLimit) {
                    $req .= " MAXSIZE UNLIMITED";
                } else {
                    $req .= " MAXSIZE {$limitSize}";
                }
            }
            $req .= " LOGGING EXTENT MANAGEMENT LOCAL AUTOALLOCATE "
                    . "BLOCKSIZE 8K SEGMENT SPACE MANAGEMENT AUTO FLASHBACK ON";
            if ($cnx->exec($req) !== false) {
                setFlash('succes', "Le tablespace {$tblspc} a bien été créé.");
                redirect(200, url('tablespace'));
            }
            setFlash('error', "Impossible d'enregistrer le nouveau tablespace {$tblspc}!<p>" . $req . "</p>");
            redirect(400, url('tablespace'));
        } else {

            $titre = 'Nouveau TABLESPACE';
            \Vue::addFileStyle('./form.css');
            \Vue::addFileStyle('./bouton.css');
            \Vue::addFileScript('./tablespace.js');
            publish(pathToTemplate('tablespace.nouveau', true));
        }
    }

    public function supprime()
    {
        checkGetCsrf();
        $cnx = \Box::get('Db');
        $tablespace = get('n', '');
        if (!empty($tablespace)) {
            set_time_limit(0);
            $drop = $cnx->exec("DROP TABLESPACE {$tablespace} INCLUDING CONTENTS AND DATAFILES");
            if ($drop !== false) {
                setFlash('succes', "Le tablespace {$tablespace} a bien été supprimé.");
                redirect(200, url('tablespace'));
            }
        }
        setFlash('error', "Impossible de supprimer le tablespace {$tablespace}!");
        redirect(400, url('tablespace'));
    }

}
