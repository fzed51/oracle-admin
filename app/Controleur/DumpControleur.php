<?php

/*
 * Copyright (c) 2016 fabien.sanchez.
 */

namespace App\Controleur;

use App\Dump;
use App\DumpManager;
use App\SchemaManager;
use function checkPostCsrf;
use function historiz;
use function html;
use function isPostMethode;
use function pathToTemplate;
use function post;
use function publish;
use function setFlash;
use function url;

/**
 * Description of DumpControleur
 *
 * @author fabien.sanchez
 */
class DumpControleur
{

    public function index()
    {
        historiz();
        if (isPostMethode()) {

            checkPostCsrf();
            $err = false;
            $dump = post('dump', '');
            if ($dump == '' || !file_exists($dump)) {
                setFlash('error', "{$dump} n'est pas valide!");
                $err = $err || true;
            } else {
                $file = basename($dump);
            }
            $type = post('dumptype', '');
            if ($type != '1' & $type != '2') {
                setFlash('error', "Le type {$type} n'est pas valide!");
                $err = $err || true;
            }
            $user = post('user', '');
            $mdp = post('mdp', $user);
            $olduser = post('olduser', '');
            $dumptype = post('dumptype');
            $remap_tablespace = post('remap_tablespace');
            echo "<pre>";
            switch ($dumptype) {
                case 1:
                    set_time_limit(0);
                    $cmd = "imp {$user}/{$mdp}@ORCL fromuser={$olduser} touser={$user} file=\"D:\\Partage\\dump\\{$file}\" ";
                    $handle = popen("$cmd 2>&1", 'r');
                    $read = stream_get_contents($handle);
                    pclose($handle);
                    echo html($read);
                    break;
                case 2:
                    $defaultTablespace = 'data';
                    $remapTablespace = '';
                    if (!is_null($remap_tablespace) && !empty($remap_tablespace)) {
                        $remapTablespace = "remap_tablespace=";
                        $remap_tablespace = explode(",", $remap_tablespace);
                        $start = 0;
                        foreach ($remap_tablespace as $key => $tablespace) {
                            $remapTablespace .= (($key > 0)?',':'') . trim($tablespace) . ':' . $defaultTablespace;
                        }
                    }
                    set_time_limit(0);
                    $cmd = "impdp {$user}/{$mdp}@ORCL dumpfile=\"{$file}\" directory=PARTAGE schemas={$olduser} remap_schema={$olduser}:{$user} {$remapTablespace}";
                    $handle = popen("$cmd 2>&1", 'r');

                    while (!feof($handle)) {
                        print fread($handle, 128);
                        flush();
                    }
                    pclose($handle);
                    break;

                default:
                    break;
            }
            echo "</pre>";
        } else {

            $titre = 'Import';
            \Vue::addFileStyle('./form.css');
            \Vue::addFileStyle('./bouton.css');
            \Vue::addFileStyle('./import.css');
            $url = url('head_dump');
            \Vue::addScript("var URL_HEAD_DUMP = '{$url}';");
            \Vue::addFileScript('import_index.js');

            $listeDump = DumpManager::getAll();
            $schemas = SchemaManager::getAll();
            $data = compact('titre', 'listeDump', 'schemas');
            publish(pathToTemplate('dump.index', true), $data);
        }
    }

    public function head()
    {
        csrfBack();
        $file = post('file');
        if (!empty($file)) {
            $dump = new Dump($file);
            echo '<pre>' . $dump->getHead() . '</pre>';
        }
    }

}
