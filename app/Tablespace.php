<?php

namespace App;

/**
 * Description of Tablespace
 *
 * @author fabien.sanchez
 */
class Tablespace
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $file;

    /**
     * @var int
     */
    public $size;

    /**
     * @var int
     */
    public $free;

    /**
     * @var int
     */
    public $increment;

    /**
     * @var int
     */
    public $maxSize;

}
