<?php

namespace App;

/**
 * Description of Schema
 *
 * @author fabien.sanchez
 */
class Schema
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $nom;

    /**
     * @var timestamp
     */
    public $createdAt;

    /**
     * @var bool
     */
    public $activ;

    /**
     * @var bool
     */
    public $dba;

    /**
     * @var string
     */
    public $tablespace;

}
