<?php

/*
 * Copyright (c) 2016 fabien.sanchez.
 */

namespace App;

/**
 * Description of Dump
 *
 * @author fabien.sanchez
 */
class Dump
{

    const DMP = "dmp";
    const DMPDP = "dmpdp";

    private $fullName;
    private $type;
    private $head;

    public function __construct($fullName)
    {
        if (!is_readable($fullName)) {
            throw new Exception("Impossible d'accéder au fichier '$fullName' !");
        }
        $this->fullName = $fullName;
        $this->type = null;
        $this->head = null;
    }

    public function getFullName()
    {
        return $this->fullName;
    }

    public function getType()
    {
        if (is_null($this->type)) {
            $head = $this->readHead();
            $strControl = substr($head, 3, 8);
            if ($strControl == "EXPORT:V") {
                $this->type = self::DMP;
            } else {
                $this->type = self::DMPDP;
            }
        }
        return $this->type;
    }

    public function getHead()
    {
        return self::binary2Hexa($this->readHead(), 32);
    }

    private function readHead()
    {
        if (is_null($this->head)) {
            $handle = fopen($this->fullName, 'r');
            $this->head = fread($handle, 512);
            fclose($handle);
        }
        return $this->head;
    }

    static private function binary2Hexa($binary, $l = 32)
    {
        $nbCars = strlen($binary);
        $pos = 0;
        $out = '';
        $hex = '';
        $str = '';
        while ($pos < $nbCars) {
            if (strlen($hex) > 0) {
                $hex.=' ';
                //$str.=' ';
            }
            $hex .= substr('00' . dechex(ord($binary[$pos])), -2);
            if (ord($binary[$pos]) >= 32) {
                $str .= "&#" . ord($binary[$pos]) . ";";
            } else {
                $str .= '?';
            }
            $pos++;
            if ($pos % $l == 0) {
                $out .= $hex . '  |  ' . $str . PHP_EOL;
                $hex = '';
                $str = '';
            }
        }
        return $out;
    }

}
