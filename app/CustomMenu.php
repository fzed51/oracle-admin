<?php

namespace App;

use fzed51\Core\Menu;
use fzed51\Core\MenuItem;
use fzed51\Core\MenuLink;
use fzed51\Core\MenuRender;

/**
 * Description of CustomMenu
 *
 * @author fabien.sanchez
 */
class CustomMenu extends MenuRender
{

    public function renderItem(MenuItem $item)
    {
        $out = "";
        $menu = false;
        if ($item->getElement() instanceof Menu) {
            $menu = true;
        }
        if (!$menu) {
            $out .= "<a href=\"" . $item->getElement()->render($this) . "\">";
        }
        $out .= "<span class=\"menu-libelle\">";
        $out .= $item->getLibelle();
        $out .= "</span>";
        if ($menu) {
            $item->getElement()->render($this);
        } else {
            $out .= "</a>";
        }
        return $out;
    }

    public function renderLink(MenuLink $link)
    {
        return $link->getLink();
    }

    public function renderMenu(Menu $menu)
    {
        $out = "<ul class=\"menu-menu\">" . PHP_EOL;
        foreach ($menu->getItems() as $item) {
            $out.= "<li class=\"menu-item\">" . $item->render($this) . "</li>" . PHP_EOL;
        }
        $out .= "</ul>" . PHP_EOL;
        return $out;
    }

}
