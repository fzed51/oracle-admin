<?php

namespace App;

/**
 * Description of SchemaManager
 *
 * @author fabien.sanchez
 */
class SchemaManager
{

    /**
     * @return \App\Schema
     */
    static public function getAll()
    {
        $cnx = \Box::get('Db');
        $out = [];
        $schema = $cnx->prepare('SELECT * FROM DBA_USERS ORDER BY USER_ID DESC');
        $activ = $cnx->prepare('SELECT COUNT(*) "NB" FROM V$SESSION WHERE USERNAME = ?');
        $schema->execute();
        while (false !== ($row = $schema->fetchObject())) {
            $tmp = new Schema();
            $tmp->id = $row->USER_ID;
            $tmp->nom = $row->USERNAME;
            $tmp->tablespace = $row->DEFAULT_TABLESPACE;
            $tmp->createdAt = $row->CREATED;
            $activ->execute([$row->USERNAME]);
            if ($activ->fetchColumn() > 0) {
                $tmp->activ = true;
            } else {
                $tmp->activ = false;
            }
            $out[] = $tmp;
        }
        return $out;
    }

    /**
     * @param string $schema_nom
     * @return boolean
     */
    static public function deconnnect($schema_nom)
    {
        $nbDeco = 0;
        $cnx = \Box::get('Db');
        $stmt_user = $cnx->prepare('SELECT SID, SERIAL# "SERIAL" FROM V$SESSION WHERE USERNAME = ?');
        $stmt_user->execute([$schema_nom]);
        while (false !== ($row = $stmt_user->fetchObject())) {
            $sid = $row->SID + 1;
            $deco = $cnx->exec("ALTER SYSTEM DISCONNECT SESSION '{$row->SID},{$row->SERIAL}'  IMMEDIATE");
            $nbDeco += ($deco !== false)?1:0;
        }
        return (bool) ($nbDeco > 0);
    }

    /**
     * @param string $schema_nom
     * @return boolean
     */
    static public function drop($schema_nom)
    {
        try {
            $cnx = \Box::get('Db');
            $stmt_user = $cnx->prepare('SELECT count(*) "NB" FROM ALL_USERS WHERE UPPER(USERNAME) = UPPER(?)');
            if (!$stmt_user) {
                print_r($cnx->errorInfo());
            }
            $stmt_user->execute([$schema_nom]);
            $nb_user = (int) $stmt_user->fetchObject()->NB;
            if ($nb_user > 0) {
                set_time_limit(0);
                $create = $cnx->exec("DROP USER {$schema_nom} CASCADE");
                return true;
            }
        } catch (PDOException $e) {
            die($e->getmessage());
        }
        return false;
    }

    /**
     * @param string $schema_nom
     * @param string $schema_mdp
     * @param boolean $dba
     * @return boolean
     */
    static public function create($schema_nom, $schema_mdp, $dba)
    {
        $cnx = \Box::get('Db');
        $stmt_user = $cnx->prepare('SELECT count(*) "NB" FROM ALL_USERS WHERE UPPER(USERNAME) = UPPER(?)');
        $stmt_user->execute([$schema_nom]);
        $nb_user = (int) $stmt_user->fetchObject()->NB;
        if ($nb_user == 0) {
            $create = $cnx->exec("CREATE USER {$schema_nom} IDENTIFIED BY {$schema_mdp} DEFAULT TABLESPACE data TEMPORARY TABLESPACE temp");
            if ($dba) {
                $create = $cnx->exec("GRANT DBA TO {$schema_nom}");
            } else {
                $create = $cnx->exec("GRANT CREATE SESSION TO {$schema_nom}");
            }
            return true;
        }
        return false;
    }

}
