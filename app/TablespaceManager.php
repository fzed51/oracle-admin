<?php

namespace App;

/**
 * Description of Tablespace
 *
 * @author fabien.sanchez
 */
class TablespaceManager
{

    static public function getAll()
    {
        $cnx = \Box::get('Db');
        $out = [];

        $tablespace = $cnx->prepare('
SELECT
    DBA_TABLESPACES.TABLESPACE_NAME "TABLESPACE",
    DBA_TABLESPACES.INITIAL_EXTENT "INITIAL_EXT",
    DBA_TABLESPACES.NEXT_EXTENT "NEXT_EXT",
    DBA_TABLESPACES.MIN_EXTENTS "MIN_EXT",
    DBA_TABLESPACES.MAX_EXTENTS "MAX_EXT",
    DBA_DATA_FILES.FILE_NAME "FILE",
    DBA_DATA_FILES.BYTES "SIZE",
    FREE_SPACE.BYTES_FREE "FREE",
    (DBA_DATA_FILES.INCREMENT_BY * DBA_TABLESPACES.BLOCK_SIZE) "INCREMENT",
    DBA_DATA_FILES.MAXBYTES "MAXSIZE"
FROM DBA_TABLESPACES
JOIN DBA_DATA_FILES
ON (DBA_TABLESPACES.TABLESPACE_NAME = DBA_DATA_FILES.TABLESPACE_NAME)
JOIN (
select TABLESPACE_NAME, sum(BYTES) "BYTES_FREE"
from dba_free_space
group by TABLESPACE_NAME
) FREE_SPACE
ON (DBA_TABLESPACES.TABLESPACE_NAME = FREE_SPACE.TABLESPACE_NAME)
');

        $tablespace->execute();
        while (false !== ($row = $tablespace->fetchObject())) {
            $tmpTablespace = new Tablespace();
            $tmpTablespace->name = $row->TABLESPACE;
            $tmpTablespace->file = $row->FILE;
            $tmpTablespace->size = $row->SIZE;
            $tmpTablespace->maxSize = $row->MAXSIZE;
            $tmpTablespace->free = $row->FREE;
            $tmpTablespace->increment = $row->INCREMENT;
            $out[] = $tmpTablespace;
        }
        return $out;
    }

}
