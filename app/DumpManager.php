<?php

/*
 * Copyright (c) 2016 fabien.sanchez.
 */

namespace App;

/**
 * Description of DumpManager
 *
 * @author fabien.sanchez
 */
class DumpManager
{

    static $extensions = [
        '*dmp*',
        '*DMP*',
        '*dump*',
        '*DUMP*'
    ];

    static public function getAll()
    {
        $dummpDir = \Config::get('dump_directory');
        $listeDumpfile = [];
        foreach (self::$extensions as $extension) {
            $listeDumpfile = array_merge($listeDumpfile, glob($dummpDir . "*." . $extension));
        }
        sort($listeDumpfile);

        $listeDump = array_map(function($item) {
            return new Dump($item);
        }, $listeDumpfile);
        return $listeDump;
    }

}
