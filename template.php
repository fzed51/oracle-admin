<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= (isset($titre))?"Oracle - $titre":"Oracle"; ?></title>
        <link rel="icon favicon" href="./favicon.ico">
        <?php
        Vue::addFileStyle('flash.css', true);
        Vue::addFileStyle('template.css', true);
        Vue::addFileStyle('normalize.css', true);
        ?>
        <?= Vue::style(); ?>
    </head>
    <body>
        <div id="master_content">
            <nav id="nav">
                <?= Vue::nav(); ?>
            </nav>
            <div id="content">
                <div class="flashs">
                    <?= getFlashs(); ?>
                </div>
                <?= Vue::content(); ?>
            </div>
        </div>
        <script src="//code.jquery.com/jquery-1.11.3.min.js" type="text/javascript"></script>
        <?php
        Vue::addFileScript('flash.js', true);
        Vue::addFileScript('helper.js', true);
        ?>
        <?= Vue::script(); ?>
    </body>
</html>
